title: Docker 101
class: animation-fade
layout: true

<!-- This slide will serve as the base layout for all your slides -->
.bottom-bar[
  {{title}}
]

---
class: impact
# {{title}}
## aneb kontejnerizace je super...
???
lolek
sss
---
# Agenda
* Klasický server vs Virtualizace
* Virtualizace
* Co je to Docker
* Instalace Dockeru
* Docker 'Hello World!!!'
---
# Server
![alt text](./assets/01-Docker/simpleserver.webp)
---
# Server
![alt text](./assets/01-Docker/bigger-server.jpg)
---
# Klasický server vs Virtualizace
![alt text](./assets/01-Docker/server.jpg)
---
# Virtualizace

## Výhody
* možnost emulace hardware včetně architektury procesoru
* definované prostředí jak potřebuji
* jednotlivé VM mohou být izolovány

## Nevýhody
* Zbytečně běží operační systémy
* Zbytečné využití ram
* Pomalý start

[https://www.redhat.com/en/topics/virtualization](https://www.redhat.com/en/topics/virtualization)
---
# Virtualizace 
## Příklad
* Virtualbox
* VMware
* QEMU
---
#Co je to Docker
**Docker neni VM**

.image-90[![alt text](./assets/01-Docker/virtualization-vs-containers_transparent.png)]
---
# Docker
## Výhody
* Startuje ihned
* Je úsporný
* izoluje aplikace
* sandbox environment
* ...

## Nevýhody
* Při vývoji dokáže docela rychle zaplnit disk
---
# Základní pojmy
- Container
  - Layers
- Image
- Volume
- Docker daemon
- Dockerfile
- Registry
---
# Kontejnery
.image-30[![alt text](./assets/01-Docker/df86f426-mobydock.png)]

Kontejneru 
- je jedno, jak je na místo dopraven
- je jedno, kde běží

**Kontejner obsahuje vše co je potřeba pro spuštění aplikace**

**Kontejner nastavení**
---
# Image
* plánek pro vytvoření kontejneru
* imutabilní - jen ke čtení
---
# Basic setup
## Instalace dockeru pro ubuntu
```bash
sudo apt install docker.io
```
## Přidání skupiny docker uživateli
* nemusím potom pořád dávat sudo
```bash
sudo usermod -aG docker $USER
reboot # nutný restart
```
---
# Kontrola Docker daemonu
## Kontrola statusu
```bash
systemctl status docker
```
## Připojení na výstup dockeru
```bash
journalctl -u docker -f  
```
---
#Docker "Hello World!!!"
```bash
docker pull hello-world
docker run hello-world
```
```bash
docker ps -a
```
```bash
docker image list
```
```bash
docker run --name muj_kontejner hello-world 
```
---
# Práce s dockerem
```bash
docker rm CONTAINER_ID
docker rm muj_kontejner
```
```bash
docker image rm hello-world
```

```bash
docker system prune --all  
```
---
# Práce s dockerem
```bash
docker run -it --name moje_ubuntu ubuntu bash
```
```bash
docker start moje_ubuntu
```
