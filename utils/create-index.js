const fs = require('fs')
const path = require('path')

const links = fs
    .readdirSync(path.resolve(__dirname, '../public'))
    .map((file) => `<a href="${file}">${file}</a>`)

const indexHtml = `
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>DevOps Academy</title>
</head>
<body>
  ${links.join('<br/>')}
</body>
</html>
`

fs.writeFileSync(path.resolve(__dirname, '../public/index.html'), indexHtml)
