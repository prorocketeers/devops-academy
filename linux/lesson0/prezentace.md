# uvod do světa Linuxu
## Vyber instalace
 - přimo na hw
    - pouze linux
    - dual boot
 - virtualizace
    - virtualbox
        - vagrant
    - vmware
    - qemu (kvm)
  - kontejenrizace
    - docker

## vyběr odnože (distribuce)
 - linuxová distribuce je nějake seskupení balíčků s linuxovým jádrem které je připraveno k instalace 
 - uživatel si jej vybere vetsinou dle doporučeni
 - za hlavnimi distribucemi stojí velké firmy jako Redhat, Suse, Canonical, ...
     - RedHat (IBM)
         - CentOS
         - Fedora
     - SuSe
     - Ubuntu
         - dědí z distribuce debian
         - plno jiných distribucí je na něm založeno, jako Linux Mint, ElementaryOS, Kali linux, ...
     - ostatní
         - alpine linux (5MB)
         …
 
## stažení distribuce, instalace z flash disku
 - nejlepe torrent file (pomáháme ostatním zájemcům k přístupu k instalaci)
 
## instalace 
 - věnování se účastníkům kurzu..
 

## intro v prubehu instalace

 - linux je pouze jádro (tvůrce Linus Torvalds 1991)
 - GNU operačni system (Richard Stallman 1983)
    aktalní systémy obsahují mnoho aplikací které se stále používají at uz na linuxu, kontejenru nebo macu,..
 - linux lze rozdělit dle distribucí
 - dříve se použival sysv init system (zavaděni ruznych demonu za ruznych kroku systemu)
 - nyni systemd (OpenRC alpine)
 - mnoho soborovych systemu 
    - ext2-4
    - btrfs
    - zfs
    - ...
    
##### vykreslovaci server
 - Xorg
 - Wayland
 - Unity

##### struktura systemu
 - / as a root
    - /home
    - /bin
    - /boot
    - /dev
    - /etc
    - /lib
    - /media
    - /mnt
    - /opt
    - /proc
    - /root
    - /sbin
    - /srv
    - /sys
    - /tmp
    - /usr
    - /var

## Prvni pouziti
 - prihlasit se, ošahat
 - vyzkoušet si ttyx pomocí alt-f2 (f1 až fx)
 - login, vysvetlit ze muzou delat i zde
 - vratit se k prostredi alt-f7 (na ubuntu pomocí alt-f2)
 - zapnout si bash

## Zakladni přikazy


##### prace s baličky
 - instalace baličku se provadi pomocí instalačních nástrojů v prostředí nebo konzoli
 - apt / yum / dnf / apk / snap / ..
 - nainstalujem si nejake ty baličky
 
 ```bash
 apt-get install mc htop
 apt-get install sl apache2
 ```
 - každy baliček ma dokumentaci pomoci přikazu `man ls`, nebo s parametrem `ls --help`

##### prace se soubory
 - ls
 - mkdir
 - grep
 - find
 - vim / nano / vi
 - mv / cp / rm / ..
 - ssh
 - mc
 - tree

vyhledani si daneho souboru v nějake složce
 ```
 ls /etc | grep apache
 ```
vyhledani vyskytu nějakeho slova v libovolnem souboru v nějake cestě
 ```
  cd /etc/apache2
  grep -R "port" .
```
vyhledani absolutní cesty k souboru nano
```
find / -name nano
```
find vyhodí plno chyb ohledně práv čtení souborů (složek), řešení:
```
find / -name nano 2>/dev/null
```
vyhledání abosulitní cesty k souboru nano, ignorovaní chyb a uložení výsledku do souboru nalezeno.txt
```
find / -name nano 2>/dev/null > ./nalezeno.txt
```

vyhledání souboru nalezeno.txt v podsložkách ve složce soubory
```
find ./soubory/ -name nalezeno.txt
```



  
##### prace se siti
 - ip (ifconfig)
 - ping
 - curl / wget
##### prace se systemem
 - mc
 - top / htop


##### porty
 - 80 / 443 / 22 / ..

## Bash.. psani skriptu
standardní výstup a standardní chybový výstup, přesměrování, pípa, návratový kód funkce
cat, grep, echo
