# Nginx

## Vysvětlení

- Webový server
- Pracuje s protokoly HTTP, HTTPS, SMTP, POP3, IMAP

## Instalace

```
sudo apt install nginx
```

- Zjistíme, zdali služba běží
- V prohlížeči (nebo curl) zobrazíme http://localhost

## Pracovní složky

- Servírované dokumenty: /var/www/html
- Nastavení vlastnictví a permisí `sudo chown -R $USER:$USER /var/www/html` a `sudo chmod -R 755 /var/www/html`
- Konfigurační soubory: /etc/nginx (hlavní konfigurační soubor `/etc/nginx/nginx.conf`)

## Nastavení reversní proxy

- Viz scripts/rule.conf
- Více informací: https://www.root.cz/clanky/nginx-jako-reverzni-proxy-pro-apache

```
server {
  listen 80 default_server;
  listen [::]:80 default_server;

  server_name localhost;
  
  location /api {
    proxy_set_header X-Forwarded-Host $host;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto $scheme;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection $http_connection;
    proxy_set_header Host $host:$server_port;
    proxy_set_header X-Forwarded-Server $host;
    proxy_pass http://127.0.0.1:3333;
  }
  location / {
    proxy_set_header X-Forwarded-Host $host;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto $scheme;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection $http_connection;
    proxy_set_header Host $host:$server_port;
    proxy_set_header X-Forwarded-Server $host;
    proxy_pass http://127.0.0.1:3331;
  }
}







```