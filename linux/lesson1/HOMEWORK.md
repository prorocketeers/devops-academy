# Homework 1

## Zadani 1

    Vyvořte bash skript, který vytvoří v definované složce náhodný počet souborů v rozmezí 1000 až 2000 se jménem xxx.txt,
    kde xxx je náhodné číslo.

## Zadani 2

    Vytvořte linuxovou službu využitím Systemd. Po zapnutí systému tato služba získá obsah stránky example.com 
    a uloží jej do souboru /tmp/{MOMENTALNI_DATUM}.temp

## Zadání 3

    Vytvořte skript v jazyce Bash, skript bude přijímat následující parametry: ./skript parametr1 parametr2
    Prvním parametrem můžou být následující slova: wget, curl. 
    Druhým parametrem bude url
    Skript při zadání ./skript wget example.com získá obsah stránky a zapíše jej do souboru wget_example.com.html
    Skript při zadání ./skript curl example.com získá obsah stránky a zapíše jej do souboru curl_example.com.html

## Zadání 4
    Vytvořte skript v jazyce Bash, skript bude spouštěn cyklicky a vždy provede zálohu všech podadresářů /home/*, pomocí 
    programu tar.

## Náhradní zadání

Napište script, který bude brát dva argumenty,
prvním argumentem bude adresa domény,
druhým agrumentem bude slovo, které hledáme

výstupem scriptu budou oddělené řádky na kterých bylo nalezeno dané slovo, pokud bude těchto záznamů více než nula, vypište výstup do souboru /tmp/vystup-DATUM.out
v případě, že nebyl daný výraz nalezen, script vypíše hlášku "Slovo $slovo nebylo na adrese $adresa nalezeno"

příklad spuštění:
vyhledavam množství divů na domeně uxes a jejich pozici
`./ukol.sh uxes.cz div`

přiklad vystupního souboru:
```
9:    <div class="container">
11:            <div class="header_content">
12:                <div class="hash">#</div>
13:                <div class="logo"></div>
14:                <div class="description">
18:                </div>
20:            </div>
23:    </div>
25:    <div class="container">
27:        <div class="contact">můžeš mě kontaktovat mailem na <a href="mailto:@localhost">adam zavináč uxes tečka cz</a></div>
30:    </div>

```
