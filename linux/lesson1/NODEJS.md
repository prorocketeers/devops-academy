# Node.js

## Vysvětlení

- Rapidní development webových aplikací (backend i frontend)
- Umožňuje spouštěť Javascript mimo webový prohližeč
- Založený na Chrome V8 Engine
- Pro správu balíčku se používá NPM (eventuelně Yarn)

## Instalace

**Node.js a NPM**

```bash
snap install node --classic --channel=10
```

**Yarn**

```bash
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt install yarn
```
