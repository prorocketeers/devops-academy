# Linux

## Výběr instalace

- Přimo na hw
  - pouze linux
  - dual boot
- Virtualizace
  - virtualbox
  - vmware
  - qemu (kvm)
- Kontejnerizace
  - docker

## Výběr distribuce

- Větsinou dle doporučeni / rozšiřeni
- RedHat (IBM)
  - CentOS
  - Fedora
- SUSE
- Ubuntu
  - parent debian
  - siblings Linux Mint, etc
- Others
  - alpine linux (5MB)
  - raspbian (Raspberry PI)

## Stažení a instalace

- Ideálně torrent

## Intro v průběhu instalace

- Linux je pouze jádro (tvůrce Linus Torvalds 1991)
- GNU operačni system (Richard Stallman 1983)
  horda ruznych přikazy, se kterými se setkame např v kontejnerech nebo MacOS
- Rozděleni dle distribuce
- Dříve se použival sysv init system (zavaděni ruznych demonu za ruznych kroku systemu)
- Nyni systemd (OpenRC alpine)
- Horda souborových systemu
  - ext2-4
  - btrfs
  - zfs
  - ...

##### vykreslovaci server

- Xorg
- Wayland

##### struktura systemu

- / (ROOT)
  - /home
  - /bin
  - /boot
  - /dev
  - /etc
  - /lib
  - /media
  - /mnt
  - /opt
  - /proc
  - /root
  - /sbin
  - /srv
  - /sys
  - /tmp
  - /usr
  - /var

## Prvni pouziti

- Přihlasit se, ošahat
- alt - f2
- Login - vysvetlit, že mužou delat i zde
- Vrátit se k prostredi alt - f7
- Zapnout si bash

## Zakladni přikazy

##### prace s baličky

- apt / yum / dnf / apk / snap / ..
- nainstalujem si nejake ty baličky

```bash
apt-get install mc htop
apt-get install sl apache2
```

- man

##### prace se soubory

- ls
- mkdir
- grep
- sed
- find
- vim / nano / vi
- mv / cp / rm / ..
- ssh
- mc

###### Zápis do souboru

```bash
cat > /tmp/file << EOF
neco
EOF
```

##### Práce se siti

- ip (ifconfig)
- ping
- curl / wget

##### Prace s baličky

- apt / yum / dnf / apk / snap / ..
- Nainstalujeme si nějaké ty baličky

```bash
apt-get install mc htop
apt-get install sl
```

- man

##### Práce se systemem

- mc
- top / htop

##### porty

- 80 / 443 (http/https)
- 22 (ssh)
- 20 / 21 (ftp)
- 25 (smtp)
- 53 (dns)
- 445 (samba)
- 546 / 547 (dhcp)

## Bash.. psani skriptu

standardní výstup a standardní chybový výstup, přesměrování, pípa, návratový kód funkce
cat, cut, grep, sort, tr, echo

. #!/bin/sh, . , for, switch, while, until, $@,$0, $1,$\$ .....
if...then...else , test, []

todo: vymyslet přiklad

## instlace javy a nodejs

nodejs

- (choose your favourite search engine) → “ubuntu install latest nodejs”

```bash
curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
sudo apt-get install nodejs
```

java

```bash
apt install openjdk-11-jdk
```

Useful shit
serve actual folder on http

```bash
python -m SimpleHTTPServer 8000
```

ssh

```
ssh-keygen
# Location is ~/.ssh/*

# apt install openssh-server
nano /etc/ssh/sshd_config
# Enable password, root
systemctl enable ssh(d)
systemctl restart ssh(d)
```

echo vs <<<

```bash
uxes@Masox ~ % <<< "kek"                                              :(
kek

uxes@Masox ~ % echo "kek"
kek

```

# Cviko 2

## Uzitecne znalosti

##### Absolutní vs relativní cesta

/home/uxes/Downloads/cosik/soubor.txt, ~/Downloads/cosik/soubor.txt
vs
./soubor.txt

##### porty

- definovane porty jsou v systemu do čisla 1000
- otevření portu nižšího než tisíc potřebuje práva root
- následující čiselne porty nejsou pevně daná, pouze zavedená

* 80 / 443 (http/https)
* 22 (ssh)
* 20 / 21 (ftp)
* 25 (smtp)
* 53 (dns)
* 445 (samba)
* 546 / 547 (dhcp)

##### zápis do souboru pomocí cat

```bash
cat > /tmp/file << EOF
neco
novy radek
vypis $promenna promenne..
ukonceni zapisu nastane po napsani magickeho eof:
EOF
```

## Bash scripting

#### program test

- využití k podmínkám

kontrola existence souboru

```bash
if [ -e /etc/passwd ]; then
  echo "Soubor existuje" >&2
else
  echo "Soubor nenalezen" >&2
fi
```

### Cron

- co je cron, k čemu je dobrý
- jake ma omezení (nejmenši časova jednotka)
- kde se nastavuje (systemově nebo per user)

### Random

- jak si vygeneruju random v bashi

### Archivace

#### program tar

- pouziti tar
- jeho připony (tar.xz, tar.gz, ..)
- rozdilne formaty rozdilna pamětova naročnost
- nepracuje s vice vlakny
- možnosti paralelizace

```
# tar
tar cfv archive.tar some-folder
tar xfv archive.tar

# tar.gz
tar zcfv archive.tar.gz some-folder
tar zxfv archive.tar.gz

# tar.bz2
tar cvfj archive.tar.bz2 some-folder
tar xvfj archive.tar.bz2
```

#### program p7zip

- použití zipu

```
sudo apt install p7zip-full

7z a archive.7z some-folder
7z x archive.7z
```

### Systémové služby

- co jsou služby
- jejich vzájemné závislosti, init levely
- k čemu se využívají
- vytvořeni služby
- co je systemd timer

### Pokud zbyde čas

Scanovani otevřených portů nějakého stroje

```bash
nmap seznam.cz

>
Starting Nmap 7.70 ( https://nmap.org ) at 2018-11-16 14:51 CET
Nmap scan report for seznam.cz (77.75.79.53)
Host is up (0.064s latency).
rDNS record for 77.75.79.53: www.seznam.cz
Not shown: 997 filtered ports
PORT    STATE  SERVICE
80/tcp  open   http
113/tcp closed ident
443/tcp open   https

Nmap done: 1 IP address (1 host up) scanned in 17.94 seconds

```

..todo

urls:

- https://cs.wikipedia.org/wiki/Cron
- https://wiki.archlinux.org/index.php/Systemd/Timers
- http://wiki.bash-hackers.org/commands/classictest
