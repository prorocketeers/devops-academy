#!/usr/bin/env bash

domain=$1
word=$2

content="`curl $domain`"
occurencies="`echo -e \"$content\" | grep -n $word | wc -l`"

echo $occurencies

if [ $occurencies -eq 0 ]; then
    echo "Slovo $word nebylo na adrese $domain nalezeno"
else
    echo $content > /tmp/output
    echo "Ulozeno do souboru /tmp/output"
fi
