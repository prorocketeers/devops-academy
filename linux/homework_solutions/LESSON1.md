# Řešení domácích úkolů 1

## Zadání 1

**script<span></span>.sh**

```bash
#!/bin/bash

# Ziskame nahodny pocet souboru
NUMBER_OF_FOLDERS=$(seq 1000 2000 | sort -R | head -n 1)

# Vytvorime slozku (v pripade, ze neexistuje)
[ -d zadani1 ] || mkdir zadani1

# Pro kazde cislo v rozsahu 1 az NUMBER_OF_FOLDERS vytvorime soubor v definovane slozce
for ((i=1; i<=NUMBER_OF_FOLDERS; i++)); do
    touch zadani1/$i
done
```

## Zadání 2

**/usr/local/bin/muj-script**

```bash
#!/bin/bash
curl uxes.cz -o "/tmp/`date +%x`.temp"
```

**/etc/systemd/system/my-service.service**

```bash
[Unit]
Description=Some testing service

[Service]
ExecStart=/usr/local/bin/muj-script

[Install]
WantedBy=multi-user.target
```

## Zadání 3

**script<span></span>.sh**

```bash
#!/bin/bash

# Pokud prvni argument neni 'curl' nebo 'wget', script ukoncime
if [ "$1" != "curl" ] && [ "$1" != "wget" ]; then
    echo "Chyba: Prvni argument musi byt 'curl' nebo 'wget'"
    exit 1
fi

# Pokud druhy argument neni predan, script ukoncime
if [ -z "$2" ]; then
    echo "Chyba: Druhy argument je pozadovan"
    exit 1
fi

if [ "$1" = "curl" ]; then
    # Ziskani prikazem 'curl'
    curl -o "curl_$2.html" $2
elif [ "$1" = "wget" ]; then
    # Ziskani prikazem 'wget'
    wget -O "wget_$2.html" $2
fi
```

## Zadání 4

**/opt/script.sh**

```bash
#!/bin/bash

# Vytvorime zalohu /home a presuneme ji do slozky /backups
DATE=$(date +"%d-%b-%Y")
cd /
tar -zcvf "home-$DATE.tar.gz" home
mv "home-$DATE.tar.gz" backups
```

**sudo crontab -e**

```
# Spusteni kazdou hodinu
0 * * * * /opt/script.sh
```
